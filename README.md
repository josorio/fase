# FASE

FASE: A Fast, Accurate, and Seamless Emulator for Custom Numerical Formats

## Getting started

### Abstract

Deep Neural Networks (DNNs) have become ubiquitous in a wide range of application domains. Despite their success, training DNNs is an expensive task which has motivated the use of reduced numerical precision formats to improve performance and reduce power consumption. Emulation techniques are a good fit to understand the properties of new numerical formats on a particular workload. However, current state-of-the-art techniques are not able to perform this tasks quickly and accurately on a wide variety of workloads.

We propose FASE, a Fast, Accurate and Seamless Emulator that leverages dynamic binary translation to enable emulation of arbitrary numerical formats. FASE is *fast*; allowing emulation of large unmodified workloads, *accurate*; emulating at instruction operand level, and *seamless*; as it does not require any code modifications and works on any application or DNN framework without any language, compiler or source code access restrictions. We evaluate FASE using a wide variety of DNN frameworks and large-scale workloads. Our evaluation demonstrates that FASE achieves better accuracy than coarser-grain state-of-the-art approaches, and shows that it is able to evaluate the fidelity of multiple numerical formats and extract conclusions on their applicability.

### Installation

FASE is based on PIN from Intel. To compile the FASE source code we will need to install PIN in our platform, to do so, please use the PIN version located in the pin folder. You just need to extract the content in the same pin folder and export a variable in the environment like the following:

```bash
export PIN_ROOT=../pin/pin-3.7-97619-g0d0c92f4f-gcc-linux
export PATH=$PIN_ROOT:$PATH
```


For further details please refer to [A BF16 FMA is All You Need for DNN Training repository](https://gitlab.bsc.es/josorio/bf16-fma). You will find there a new numerical datatype using FASE/SERP as the emulation platform.

If you have any questions please do not hesitate to contact: [John Osorio](mailto:john.osorio.rios@intel.com?subject=[GitLab]%20Source%20FASE%20Tool)
