import torch
seed = 3
torch.manual_seed(seed)

import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from models import *

import torchvision
import torchvision.transforms as transforms
import os
import argparse
import time

def parse_args():
    parser = argparse.ArgumentParser(description='ResNet Training on CIFAR 100')
    parser.add_argument('--model', help='Name of the model ResNet18, ResNet34, ResNet50, ResNet101', default='ResNet18')
    parser.add_argument('--weight_decay', help='Float value to set the weight decay parameter in the SGD optimizer', type=float, default=0.0)
    parser.add_argument('--milestones', help='List of epochs to change the Learning Rate', type=int, nargs='+', default=[80,120])
    parser.add_argument('--chk', help='Filename to save the checkpoints during training')
    parser.add_argument('--training_data', help='PATH to load the data to use during training', default='./')
    parser.add_argument('--epochs_to_train', help='Number of epochs to train', default=160, type=int)
    parser.add_argument('--enable_mp', help='Do the training using the MP approach', default=False, type=bool)
    args = parser.parse_args()
    return args


def prepare_data(args):
    print("Preparing the Data ...", flush=True)
    transform_train = transforms.Compose([
        transforms.RandomCrop(32, padding=4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.507, 0.487, 0.441), (0.267, 0.256, 0.276)),
        #transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ])

    trainset = torchvision.datasets.CIFAR100(
        root=args.training_data, train=True, download=True, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(
        trainset, batch_size=128, shuffle=True, num_workers=2)

    return trainloader


def save_checkpoint(filename, epoch, net, optimizer, scheduler):
    state = {
        'net': net.state_dict(),
        'epoch': epoch,
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
    }
    torch.save(state, filename+'_'+str(epoch+1)+'.pth')


def attachFMA_BF16():
    path = 'fifopipe'
    fifo = open(path, 'w')
    fifo.write('A\n')
    fifo.close()
    time.sleep(20)

def attachFMA_MP():
    path = 'fifopipe'
    fifo = open(path, 'w')
    fifo.write('B\n')
    fifo.close()
    time.sleep(20)


def train(args):
    if args.model == 'ResNet18':
        net = ResNet18(num_classes=100)
    elif args.model == 'ResNet34':
        net = ResNet34(num_classes=100)
    elif args.model == 'ResNet50':
        net = ResNet50(num_classes=100)
    elif args.model == 'ResNet101':
        net = ResNet101(num_classes=100)

    device = 'cpu'
    net = net.to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.1,
                          momentum=0.9, weight_decay=args.weight_decay)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=args.milestones, gamma=0.1) # Ultra Low uses milestones=[82,122]

    trainloader = prepare_data(args)
    net.train()
    start_epoch = 0

    if args.enable_mp:
        print("Enabling MP Training")
        attachFMA_MP()
    else:
        # The BF16xN approache used depends
        # on the pintool being invoked in the
        # Launch script.
        print('Enabling BF16 Training')
        attachFMA_BF16()

    print("Training starts ...", flush=True)
    print('Epoch,','T.Loss', flush=True)

    for epoch in range(start_epoch, start_epoch+args.epochs_to_train):
        # Decay the learning rate when needed
        train_loss = 0
        for batch_idx, (inputs, targets) in enumerate(trainloader):
            inputs, targets = inputs.to(device), targets.to(device)
            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs, targets)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
        print(epoch+1, ',', train_loss/(batch_idx+1), flush=True)
        scheduler.step()
        save_checkpoint(args.chk, epoch, net, optimizer, scheduler)
    print('Training Finished ...')


def main():
    args = parse_args()
    train(args)


if __name__ == '__main__':
    main()
